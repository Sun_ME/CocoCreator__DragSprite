import { Game } from "./Game";

const { ccclass, property } = cc._decorator;
@ccclass
export class Player extends cc.Component {
    @property(cc.Label)
    lable: cc.Label = null;
    @property(cc.Float)
    count: number = 50.2;
    @property(Game)
    game: Game = null;
    @property(cc.Node)
    labelNode: cc.Node;
    //移动的目标位置
    moveToPos:cc.Vec2;
    //当前鼠标位置相对于要拖动对象的偏移量
    offset:cc.Vec2;
    //鼠标刚按下时的坐标位置
    lastPos:cc.Vec2;

    onLoad() {
        //console.log(this.game.count);
        this.labelNode.on(cc.Node.EventType.TOUCH_START, (e) => {
            console.log(e);
            let touches=e.getTouches();
            let touchLoc=touches[0].getLocation(); 
            this.lastPos=touchLoc;
            this.moveToPos=this.labelNode.parent.convertToNodeSpaceAR(touchLoc);
            this.offset=this.labelNode.getPosition().sub(this.moveToPos);
        }, this);
        this.labelNode.on(cc.Node.EventType.TOUCH_MOVE, (e) => {
            console.log(e);
            let touches=e.getTouches();
            let touchLoc=touches[0].getLocation(); 
            this.moveToPos=this.labelNode.parent.convertToNodeSpaceAR(touchLoc);
            //this.labelNode.setPosition(this.moveToPos);
            this.labelNode.setPosition(this.moveToPos.add(this.offset));
        }, this);
    }
    start() {
       
    }
    update(dt: number) {
        //console.log(dt);
    }
    lateUpdate() {

    }
}